package gov.uk.cart;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import gov.uk.cart.dao.CustomerBillDao;
import gov.uk.cart.dto.Item;
import gov.uk.cart.service.ShoppingCartGenerator;
import gov.uk.cart.utils.ItemEnum;
import junit.framework.Assert;

/**
 * 
 * @author vineethtshetty
 *
 */
public class ShoppingCartTest {

    ShoppingCartGenerator shoppingCart;
    static BigDecimal appleCost = new BigDecimal(0.60);
    static BigDecimal orangeCost = new BigDecimal(0.25);

    @Before
    public void setUp() {
        shoppingCart = new ShoppingCartGenerator();
    }

    @Test
    public void testShoppingCartIsEmpty() {
        Assert.assertEquals(shoppingCart.getShoppingItems(), new ArrayList<Item>());
    }

    @Test
    public void testOnlyOneItemInCart() {
        shoppingCart.addShoppingItem(new Item(ItemEnum.Apple.toString(), 1, appleCost));

        int numberOfItems = shoppingCart.getShoppingItems().size();

        Assert.assertEquals(numberOfItems, 1);
    }
    
    @Test
    public void testItemsInCart() {
        shoppingCart.addShoppingItem(new Item(ItemEnum.Apple.toString(), 1, appleCost));
        shoppingCart.addShoppingItem(new Item(ItemEnum.Orange.toString(), 1, orangeCost));

        int numberOfItems = shoppingCart.getShoppingItems().size();

        Assert.assertEquals(numberOfItems, 2);
    }
    
    @Test
    public void testTotalCartValue() {
        shoppingCart.addShoppingItem(new Item(ItemEnum.Apple.toString(), 3, appleCost));
        shoppingCart.addShoppingItem(new Item(ItemEnum.Orange.toString(), 2, orangeCost));

        shoppingCart.generateBill(new CustomerBillDao());
        
        Assert.assertEquals(shoppingCart.getTotalBill().setScale(2, RoundingMode.HALF_UP), 
        		new BigDecimal(1.70).setScale(2, RoundingMode.HALF_UP));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testItemNotInCart() {
        shoppingCart.addShoppingItem(new Item("Grapes", 1, BigDecimal.valueOf(0.15)));

        for (Item item : shoppingCart.getShoppingItems()) {
        	Assert.assertEquals(false, ItemEnum.valueOf(item.getName()));
        }

    }
   
    @Test(expected = IllegalArgumentException.class)
    public void testItemWhichIsNotSold() {
        shoppingCart.addShoppingItem(new Item("Grapes", 4, orangeCost));
        shoppingCart.generateBill(new CustomerBillDao());

        Assert.assertEquals(shoppingCart.getTotalBill(), 1.0);
    }
    
    /**
     * test case after offer applied.
     * 5 apple cost without discount = £3 
     * 5 apple cost after discount =  £1.80
     * Buy one get one free offer has been applied 
     */
    @Test
    public void testTotalForAppleItemInCart_After_Offer_Five_Apples() {
        shoppingCart.addShoppingItem(new Item(ItemEnum.Apple.toString(), 5, appleCost));

        shoppingCart.generateBill(new CustomerBillDao());
        
        Assert.assertEquals(shoppingCart.getTotalBill().setScale(2, RoundingMode.HALF_UP), 
        		new BigDecimal(1.80).setScale(2, RoundingMode.HALF_UP));
    }
    
    /**
     * test case after offer applied.
     * 6 apple cost without discount = £1.5
     * 6 apple cost after discount =  £1
     * Buy one get one free offer has been applied 
     */
    @Test
    public void testTotalForOrangeItemInCart_After_Offer_Six_Oranges() {
        shoppingCart.addShoppingItem(new Item(ItemEnum.Orange.toString(), 6, orangeCost));

        shoppingCart.generateBill(new CustomerBillDao());
        
        Assert.assertEquals(shoppingCart.getTotalBill().setScale(2, RoundingMode.HALF_UP), 
        		new BigDecimal(1).setScale(2, RoundingMode.HALF_UP));
    }
    
    /**
     * test case after offer applied.
     * 6 apple cost without discount = £1.5
     * 6 apple cost after discount =  £1
     * Checking not same as expected was £1.5 but actual was £1
     * Buy one get one free offer has been applied 
     */
    @Test
    public void testTotalForOrangeItemInCart_TestCase_Failed_When_Expecting_Cost_Without_Discount() {
        shoppingCart.addShoppingItem(new Item(ItemEnum.Orange.toString(), 6, orangeCost));

        shoppingCart.generateBill(new CustomerBillDao());
        
        Assert.assertNotSame(shoppingCart.getTotalBill().setScale(2, RoundingMode.HALF_UP), 
        		new BigDecimal(1.50).setScale(2, RoundingMode.HALF_UP));
    }
    
    /**
     * test case after offer applied.
     * 5 apple cost without discount = £3 
     * 5 apple cost after discount =  £1.80
     * Checking not same as expected was £3 but actual was £1.80
     * Buy one get one free offer has been applied 
     */
    @Test
    public void testTotalForAppleItemInCart_TestCase_Failed_When_Expecting_Cost_Without_Discount() {
        shoppingCart.addShoppingItem(new Item(ItemEnum.Apple.toString(), 5, appleCost));

        shoppingCart.generateBill(new CustomerBillDao());
        
        Assert.assertNotSame(shoppingCart.getTotalBill().setScale(2, RoundingMode.HALF_UP), 
        		new BigDecimal(3.0).setScale(2, RoundingMode.HALF_UP));
    }
}
