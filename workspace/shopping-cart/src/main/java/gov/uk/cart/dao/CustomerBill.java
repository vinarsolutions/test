package gov.uk.cart.dao;

import gov.uk.cart.dto.Item;
import gov.uk.cart.service.ShoppingCartGenerator;

/**
 * Interface for customer bill genration
 * @author vineethtshetty
 */
public interface CustomerBill {
    void customerShoppingCart(ShoppingCartGenerator shoppingCart);

	void customerCartItem(Item item);
}
 