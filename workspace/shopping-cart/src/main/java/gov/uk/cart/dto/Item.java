package gov.uk.cart.dto;

import java.math.BigDecimal;

import gov.uk.cart.service.ShoppingCartGenerator;

/**
 * @author vineethtshetty
 */
public class Item{
    private String name;
    private int quantity;
    private BigDecimal price;

    private ShoppingCartGenerator shoppingCart;

    public Item(String name, int quantity, BigDecimal price) {
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public ShoppingCartGenerator getShoppingCart() {
        return shoppingCart;
    }

    public void setShoppingCart(ShoppingCartGenerator shoppingCart) {
        this.shoppingCart = shoppingCart;
    }
}
