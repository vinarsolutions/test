package gov.uk.cart.service;

import gov.uk.cart.dao.CustomerBill;

/**
 * @author vineethtshetty
 */
public interface ShoppingCart {
    public void generateBill(CustomerBill customerBill);
}
