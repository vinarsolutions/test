package gov.uk.cart.utils;


public enum ItemEnum {
    Apple ("Apple"),
    Orange ("Orange");

    private final String value;

    private ItemEnum(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

}
