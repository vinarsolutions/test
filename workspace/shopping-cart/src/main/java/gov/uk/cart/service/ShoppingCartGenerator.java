package gov.uk.cart.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import gov.uk.cart.dao.CustomerBill;
import gov.uk.cart.dto.Item;

public class ShoppingCartGenerator implements ShoppingCart{

	private BigDecimal totalBill = new BigDecimal(0.0);

    private List<Item> shoppingItems = new ArrayList<Item>();

    public void addShoppingItem(Item shoppingCartItem) {
        shoppingCartItem.setShoppingCart(this);
        shoppingItems.add(shoppingCartItem);
    }

    public List<Item> getShoppingItems() {
        return shoppingItems;
    }

    public void setShoppingItems(List<Item> shoppingItems) {
        this.shoppingItems = shoppingItems;
    }

    public BigDecimal getTotalBill() {
        return totalBill;
    }

    public void setTotalBill(BigDecimal totalBill) {
        this.totalBill = totalBill;
    }

    public void generateBill(CustomerBill customerBill) {
    	customerBill.customerShoppingCart(this);
	}
}
