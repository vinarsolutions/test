package gov.uk.cart.dao;

import gov.uk.cart.dto.Item;
import gov.uk.cart.service.ShoppingCartGenerator;
import gov.uk.cart.utils.ItemEnum;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Class handling all calculation for bill generation.
 * @author vineethtshetty
 *
 */
public class CustomerBillDao implements CustomerBill {
	
	int newQuantity = 0;
	boolean itemDoesNotExist = false;
    /**
     * Method used for calculation total price of selected items.
     * @param shoppingCart
     */
	public void customerShoppingCart(ShoppingCartGenerator shoppingCart) {
		for (int i = 0; i < shoppingCart.getShoppingItems().size(); i++) {
        	customerCartItem(shoppingCart.getShoppingItems().get(i));
        }

        System.out.println("Amount to pay : £" + shoppingCart.getTotalBill().setScale(2, RoundingMode.HALF_UP));
	}

	/**
	 * Loping through items and calculation price after offer or discount.
	 * @param item
	 */
   public void customerCartItem(Item item) {
        //Calculation of offer for apple and oranges.
        OfferCalculation(item);
        if (!itemDoesNotExist) {
        	BigDecimal itemCost  = item.getPrice().multiply(BigDecimal.valueOf(newQuantity));

        	item.getShoppingCart().setTotalBill(item.getShoppingCart().getTotalBill().add(itemCost));
        }else {
            throw new IllegalArgumentException("No such Item in Stock!");
        }
    }


   /**
    * Calculate offer based on business rule.
    * The shop decides to introduce two new offers
    * buy one, get one free on Apple
    * 3 for the price of 2 on Oranges
    * @param item
    */
   private void OfferCalculation(Item item) {
       newQuantity = item.getQuantity();

       switch (ItemEnum.valueOf(item.getName())) {
           case Apple:
               // checking buy 1 get 1 offer
               //then 1 will be need to added explicitly, Ex: if there is 2 apples charge customer for 1 apple.
               if (item.getQuantity() > 1) {
                   newQuantity = item.getQuantity() - (item.getQuantity() / 2);
               }
               break;

           case Orange:
               // checking 3 for the price of 2
               if (item.getQuantity() > 2) {
                   newQuantity = item.getQuantity() - (item.getQuantity() / 3);
               }
               break;

           default:
        	   itemDoesNotExist = true;
               break;
       }
   }

}
